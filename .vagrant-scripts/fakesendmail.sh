#!/bin/sh 
mailpath="/vagrant/.mail"
if ! [ -d $mailpath ]; then
    mkdir -p $mailpath
fi

if [ ! -f $mailpath/num ]; then 
	echo "0" > $mailpath/num 
fi 
num=`cat $mailpath/num` 
num=$(($num + 1)) 
echo $num > $mailpath/num 

name="$mailpath/letter_$num.eml"
while read line 
do 
	echo $line >> $name
done 
chmod 777 $name
/bin/true
