#!/usr/bin/env bash

# Variables
site_root="/vagrant/www/public"
site_log_dir="/vagrant/.tmp/log/nginx"
hostname="${1}"

php_memory="128M"
php_timezone="Europe/London"

mysql_data_dir="/vagrant/.mysqldata"
mysql_dbname="main"
mysql_dbname_tests="test"
mysql_user="root"
mysql_password="root"

# ...

add-apt-repository -y ppa:ondrej/php

apt-get -y update

apt-get install -y git mc curl wget python-software-properties

# Directory public
mkdir -p ${site_root} ${site_log_dir}
if [ `ls ${site_root} | wc -l` -eq 0 ]
then
    echo "<?php phpinfo(); ?>" > "${site_root}/index.php"
fi

# PHP7.3 & Nginx
apt-get install -y nginx
apt-get install -y php7.3-fpm php7.3-cli php7.3-bcmath php7.3-mbstring php7.3-curl php7.3-gd php7.3-sqlite3 php7.3-zip php7.3-mysql php7.3-imap php7.3-imagick php7.3-xml php7.3-intl php7.3-xdebug

# MCRYPT
#apt-get install -y php7.3-dev
#apt-get -y install gcc make autoconf libc-dev pkg-config
#apt-get -y install libmcrypt-dev
#apt-get -y install php-pear
#pecl channel-update pecl.php.net
#echo '' | pecl install mcrypt-1.0.3
#echo extension=mcrypt.so > /etc/php/7.3/mods-available/mcrypt.ini
#ln -fs /etc/php/7.3/mods-available/mcrypt.ini /etc/php/7.3/cli/conf.d/20-mcrypt.ini
#ln -fs /etc/php/7.3/mods-available/mcrypt.ini /etc/php/7.3/fpm/conf.d/20-mcrypt.ini

#apt-get -y install php-ssh2
# libssh2 installing with libgcrypt instead of openssl.
# If php throws an error "ssh2_auth_pubkey_file(): Authentication failed for ... using public key",
# this command may convert private key file to PEM format:
# openssl rsa -in priv.openssh -out priv.pem

# Add user "vagrant" to group www-data and vice versa
usermod -a -G www-data vagrant
usermod -a -G vagrant www-data

cp /vagrant/.vagrant-scripts/xdebug/xdebug.ini /etc/php/7.3/mods-available/xdebug.ini
cp /vagrant/.vagrant-scripts/nginx/default /etc/nginx/sites-available/default
sed -i "s/server_name .*/server_name $hostname\;/" /etc/nginx/sites-available/default

sed -i 's/post_max_size = 8M/post_max_size = '$php_memory'/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = '$php_memory'/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 86400/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/disable_functions = [a-z_,]\+/disable_functions = /g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/display_errors = Off/display_errors = On/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/display_startup_errors = Off/display_startup_errors = On/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g' "/etc/php/7.3/fpm/php.ini"
sed -i 's%;error_log = php_errors.log%error_log = '$site_log_dir'/php_errors.log%g' "/etc/php/7.3/fpm/php.ini"

sed -i 's/post_max_size = 8M/post_max_size = '$php_memory'/g' "/etc/php/7.3/cli/php.ini"
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = '$php_memory'/g' "/etc/php/7.3/cli/php.ini"
sed -i 's/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 86400/g' "/etc/php/7.3/cli/php.ini"
sed -i 's/disable_functions = [a-z_,]\+/disable_functions = /g' "/etc/php/7.3/cli/php.ini"
sed -i 's/display_errors = Off/display_errors = On/g' "/etc/php/7.3/cli/php.ini"
sed -i 's/display_startup_errors = Off/display_startup_errors = On/g' "/etc/php/7.3/cli/php.ini"
sed -i 's/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g' "/etc/php/7.3/cli/php.ini"
sed -i 's%;error_log = php_errors.log%error_log = '$site_log_dir'/php_errors.log%g' "/etc/php/7.3/cli/php.ini"

# Mail stub
sed -i 's%;sendmail_path =%sendmail_path = /vagrant/.vagrant-scripts/fakesendmail.sh%g' "/etc/php/7.3/fpm/php.ini"
sed -i 's%;sendmail_path =%sendmail_path = /vagrant/.vagrant-scripts/fakesendmail.sh%g' "/etc/php/7.3/cli/php.ini"
chmod 755 "/vagrant/.vagrant-scripts/fakesendmail.sh"

# MySQL
echo "mysql-server mysql-server/root_password password ${mysql_password}" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password ${mysql_password}" | debconf-set-selections

apt-get -y install mysql-server mysql-client
service mysql stop
sed -i 's%User=mysql%User=vagrant%g' "/lib/systemd/system/mysql.service"
sed -i 's%Group=mysql%Group=vagrant%g' "/lib/systemd/system/mysql.service"

sed -i 's%# alias /var/lib/mysql/ -> /home/mysql/,%alias /var/lib/mysql/ -> '$mysql_data_dir'/,%g' "/etc/apparmor.d/tunables/alias"
service apparmor restart

if ! [ -L /var/lib/mysql ]; then
  if ! [ -d ${mysql_data_dir} ]; then
    mv /var/lib/mysql ${mysql_data_dir}
  else
    rm -rf /var/lib/mysql
  fi
  ln -fs ${mysql_data_dir} /var/lib/mysql
fi
find / -user mysql -exec chown vagrant:vagrant '{}' \;
systemctl daemon-reload
service mysql start

mysql_upgrade --user=${mysql_user} --password=${mysql_password} --force
service mysql restart

mysql -h localhost -u${mysql_user} -p${mysql_password} -e "CREATE DATABASE IF NOT EXISTS \`${mysql_dbname}\` CHARACTER SET utf8 COLLATE utf8_general_ci"
mysql -h localhost -u${mysql_user} -p${mysql_password} -e "CREATE DATABASE IF NOT EXISTS \`${mysql_dbname_tests}\` CHARACTER SET utf8 COLLATE utf8_general_ci"

# PHPMyAdmin
sed -i 's/user     = [0-9A-Za-z_-]\+/user     = '$mysql_user'/g' "/etc/mysql/debian.cnf"
sed -i 's/password = [0-9A-Za-z_-]\+/password = '$mysql_password'/g' "/etc/mysql/debian.cnf"
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password ${mysql_password}" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password ${mysql_password}" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password ${mysql_password}" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
apt-get -y install phpmyadmin
cp /vagrant/.vagrant-scripts/nginx/pma /etc/nginx/sites-available/pma
ln -s /etc/nginx/sites-available/pma /etc/nginx/sites-enabled/pma
ln -s /usr/share/phpmyadmin /usr/share/nginx/html

# Set Timezone
rm -rf /etc/localtime
ln -fs /usr/share/zoneinfo/${php_timezone} /etc/localtime
sed -i 's%;date.timezone =%date.timezone = "'$php_timezone'"%g' "/etc/php/7.3/fpm/php.ini"
sed -i 's%;date.timezone =%date.timezone = "'$php_timezone'"%g' "/etc/php/7.3/cli/php.ini"

# Install Node.js
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Chmod install scripts
chmod +x "/vagrant/.vagrant-scripts/project-setup.sh"
chmod +x "/vagrant/npm"
chmod +x "/vagrant/artisan"
chmod +x "/vagrant/.vagrant-scripts/tools/webhooks-proxy.sh"
# Custom sh script's execution
if [ -f "./customize.sh" ]; then
 ./customize.sh
fi

if ! [ -f "/home/vagrant/.npmrc" ]; then
  echo '//registry.npmjs.org/:_authToken=7805ebf9-cd55-4f4b-b83c-b0e6b79b4010' > /home/vagrant/.npmrc
fi