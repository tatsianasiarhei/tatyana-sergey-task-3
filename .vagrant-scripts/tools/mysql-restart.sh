#!/usr/bin/env bash

service mysql stop
sed -i 's%User=mysql%User=vagrant%g' "/lib/systemd/system/mysql.service"
sed -i 's%Group=mysql%Group=vagrant%g' "/lib/systemd/system/mysql.service"
#find / -user mysql -exec chown vagrant:vagrant '{}' \;
systemctl daemon-reload
service mysql start
