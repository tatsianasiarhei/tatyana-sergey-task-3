#!/usr/bin/env bash

if [ -f "/vagrant/www/vendor/spurit/shopify-webhooks/composer.json" ]; then
    flock -n /tmp/webhooks-proxy-receive.lock php /vagrant/www/artisan webhooks:proxy-receive
fi
