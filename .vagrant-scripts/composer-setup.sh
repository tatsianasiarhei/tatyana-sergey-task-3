#!/usr/bin/env bash

chmod +x "/vagrant/composer"

# For getting git repositories
apt-get -y install git

EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --install-dir=/usr/local/bin --filename=composer --quiet
RESULT=$?
rm composer-setup.php

# Bitbucket auth
bitbucketKey="fuvErfCk6NH39dmZwF"
bitbucketSecret="aAMBtg4muVr86u7stZW9wqnATmNUnWFf"

mkdir -p "/home/vagrant/.config/composer/"
if ! [ -f "/home/vagrant/.config/composer/auth.json" ]; then
  echo '{"bitbucket-oauth":{"bitbucket.org":{"consumer-key":"'$bitbucketKey'","consumer-secret":"'$bitbucketSecret'"}}}' > /home/vagrant/.config/composer/auth.json
fi
chown vagrant:vagrant -R /home/vagrant/.config
# END bitbucket auth

exit $RESULT