#!/usr/bin/env bash

chmod -R 777 /vagrant/www/storage
chmod -R 777 /vagrant/www/bootstrap/cache

INSTALL=0
if ! [ -f "/vagrant/www/.env" ]; then
  INSTALL=1
  cp "/vagrant/www/.env.example" "/vagrant/www/.env"
fi

if ! [ -d "/vagrant/www/vendor" ]; then
    cd /vagrant/www && composer install --no-ansi
fi

if [ "$INSTALL" -eq 1 ]; then
    cd /vagrant/www && php artisan key:generate
fi

if ! [ -f "/vagrant/www/config/core.php" ]; then
    cd /vagrant/www && php artisan vendor:publish --provider="Spurit\Core\CoreServiceProvider"
fi

php /vagrant/www/artisan migrate
php /vagrant/www/artisan db:seed

php /vagrant/www/artisan ide-helper:generate
#php /vagrant/www/artisan ide-helper:models -n
php /vagrant/www/artisan ide-helper:meta

if ! [ -d "/vagrant/www/node_modules" ]; then
    cd /vagrant/www && npm install --no-optional && npm run dev
fi

{ echo "* * * * * /vagrant/.vagrant-scripts/tools/webhooks-proxy.sh"; } | crontab -
