<?php

use App\Http\Controllers;
use App\Providers\RouteServiceProvider;

Route::middleware('auth')->get('/', function () {
    return redirect('/admin');
});

Route::middleware('guest')->get('auth/install', Controllers\AuthController::class . '@install')->name('auth.install');

Route::middleware('auth')
    ->prefix('/admin')
    ->group(function () {
        Route::prefix('/pc')->group(function () {
            Route::post('/first-install', Controllers\PluginCenterController::class . '@notifyAboutFirstInstall');
        });

        Route::get('/', Controllers\AdminController::class . '@main');

        Route::prefix(RouteServiceProvider::API_ROUTES_PREFIX)->group(function () {
        });
        Route::get('{any_route}', Controllers\AdminController::class . '@main')->where('any_route', '.*');
    });

Route::middleware('auth')->get('/payment', Controllers\AdminController::class . '@main');

Route::group(['prefix' => 'products-settings'], function () {
    Route::get('/{shopId}', Controllers\Setting\ProductStockSettingController::class . '@getProductsSettings');
    Route::post('/update/{shopId}', Controllers\Setting\ProductStockSettingController::class . '@updateProductsSettings');
});
Route::group(['prefix' => 'global-settings'], function () {
    Route::get('/{shopId}', Controllers\Setting\GlobalSettingController::class . '@getGlobalSettings');
    Route::post('/update/{shopId}', Controllers\Setting\GlobalSettingController::class . '@updateGlobalSettings');
});
