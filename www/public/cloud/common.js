(function() {
  let shop = {
    AwsS3Path: '//s3.amazonaws.com/dev-shopify-apps/task-3-tatyana-sergey'
  }

  let loadScript = function (url, type, callback) {
    let script = document.createElement("script");
    script.type = type;
    script.onload = function () {
      callback();
    };
    script.onerror = function() {
      alert("Error loading " + url);
    };
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  };

  let loadStyle = function(url, type){
    let tag = document.createElement('link');
    tag.type = type;
    tag.href = url;
    tag.rel = 'stylesheet';

    document.getElementsByTagName("head")[0].appendChild(tag);
  };

  let getUrlParameter = function (sParam) {
    let sPageURL = decodeURIComponent(window.location.search.substring(1));
    let sURLVariables = sPageURL.split('&');

    for (let i = 0; i < sURLVariables.length; i++) {
      let sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };

  shop.init = function ($) {
    loadStyle(shop.AwsS3Path + '/common.css', 'text/css');
    loadScript(shop.AwsS3Path + '/store/' + Spurit.Sampleapp.snippet.shopHash + '.js?', "text/javascript", function () {
      shop.run($);
    });
  };

  shop.run = function ($) {
    let Config = (function () {
      return {
        getSettingsByProductId: function(productId) {
          let productObject = Spurit.Sampleapp.settings['products_settings'].find(product => {
            return product.product_id === productId
          })
          if (typeof productObject !== undefined) return productObject;
          return Spurit.Sampleapp.settings['global_settings'];
        }
      }
    })(ShopifyProduct);

    let ProgressBar = function(product, config) {
      this.showProgressBar = function() {
        let progressBarSettings = this.getSettingsForProgressBar();
        let progressHtml = document.createElement('div');
        progressHtml.setAttribute("class", "progress" );
        progressHtml.innerHTML = ('<progress value="'+ progressBarSettings.progress +'" max="100" class="' + progressBarSettings.styleClass + '"' + '>' + '</progress>');
        if (progressBarSettings.isShowCount) {
          progressHtml.innerHTML += ('<div class="show-real-count">Products in stock: ' + progressBarSettings.count + '</div>');
        }
        return progressHtml;
      };

      this.updateProgressBar = function() {
        $('.progress').replaceWith(this.showProgressBar());
      };

      this.getSettingsForProgressBar = function(){
        let productVariandId = getUrlParameter('variant');
        let settings = config.getSettingsByProductId(product.id);
        let count = product.variants[productVariandId].inventory_quantity ? product.variants[productVariandId].inventory_quantity : settings.high_count;

        return {
          count: count,
          isShowCount: settings.show_real_count,
          progress: this.getProgressResult(count, settings.high_count),
          styleClass: count >= settings.high_coun ? 'high-count' : 'low-count',
        }
      };

      this.getProgressResult = function (count, max_count) {
        let progress =  Math.round(count * 100 / max_count);
        return progress > 100 ? 100 : progress;
      };

      $('.product-single__title').append(this.showProgressBar());
    };

    let progressBar = new ProgressBar(ShopifyProduct.product, Config);

    $('.product-single').change('.single-option-selector', function () {
      progressBar.updateProgressBar();
    });
  };

  shop.init(jQuery);

})();