<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableValueToProductsStockSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_stock_settings', function (Blueprint $table) {
            $table->integer('low_count')->nullable()->change();
            $table->integer('high_count')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_stock_settings', function (Blueprint $table) {
            $table->integer('low_count')->change();
            $table->integer('high_count')->change();
        });
    }
}
