<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->string('domain_front')->nullable()->after('domain');
            $table->string('currency', 3)->default('USD')->after('token');
            $table->string('money_format')->nullable()->after('currency');
            $table->string('timezone')->nullable()->after('money_format');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->dropColumn('domain_front');
            $table->dropColumn('currency');
            $table->dropColumn('money_format');
            $table->dropColumn('timezone');
        });
    }
}
