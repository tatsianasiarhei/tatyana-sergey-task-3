<?php

namespace App\Providers;

use App\Services\AutoSelectorService;
use Illuminate\Support\ServiceProvider;
use Barryvdh\LaravelIdeHelper;
use Spurit\RestProxyConnector\RestProxyConnector;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        umask(0);

        if ($this->app->environment() !== 'production') {
            $this->app->register(LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->singleton(RestProxyConnector::class, function ($app) {
            return new RestProxyConnector(
                new \GuzzleHttp\Client()
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app(AutoSelectorService::class)->boot();

        //$this->logDbQueries();

        // Fix for old MySql
        //\Schema::defaultStringLength(191);
    }

    /**
     *
     */
    private function logDbQueries(): void
    {
        \DB::listen(function ($query) {
            // $query->sql
            // $query->bindings
            // $query->time
            file_put_contents(
                __DIR__ . '/sql.log',
                date('r') . PHP_EOL . $query->time . PHP_EOL . $query->sql . PHP_EOL . json_encode($query->bindings) . PHP_EOL . PHP_EOL,
                FILE_APPEND
            );
        });
    }
}
