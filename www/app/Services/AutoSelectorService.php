<?php
namespace App\Services;

use App\Models\Setting\Settings;
use Spurit\SelectorPicker\Autoselector;
use Spurit\SentryAdapter\SentryAdapter;

/**
 * Class AutoSelectorService
 * @package App\Http\Services
 * @author Igor Razumovsky <igor.r@spur-i-t.com>
 */
class AutoSelectorService
{
    /**
     *
     */
    private const SELECTOR_TAGS = [
        'add-to-cart' => 'product-add-to-cart'
    ];

    /**
     * @var Autoselector
     */
    private $autoSelector;

    /**
     *
     */
    public function boot(): void
    {
        Settings::creating(function (Settings $setting) {
            $config = array_replace_recursive(
                Settings::getConfigDefault(),
                $this->getAutoSelectors()
            );
            $setting->config = $config;
        });
    }

    /**
     * AutoSelectorService constructor.
     * @param Autoselector $autoSelector
     */
    public function __construct(Autoselector $autoSelector)
    {
        $this->autoSelector = $autoSelector;
    }

    /**
     * @return array
     */
    public function getAutoSelectors(): array
    {
        $selectors = [];
        try {
            $auto = $this->autoSelector->get(...array_values(self::SELECTOR_TAGS));
            foreach (self::SELECTOR_TAGS as $key => $tag) {
                if (!isset($auto[$tag])) {
                    continue;
                }
                $selectors[$key] = [
                    'auto'     => true,
                    'selector' => $auto[$tag]->selector,
                ];
            }
        } catch (\Exception $e) {
            SentryAdapter::sendException($e);
            \Log::error($e->getMessage() . $e->getTraceAsString());
        }

        return $selectors;
    }
}