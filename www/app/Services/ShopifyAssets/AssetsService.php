<?php

declare(strict_types=1);

namespace App\Services\ShopifyAssets;

use Spurit\ShopifyApi\Api as ShopifyApi;
use App\Models\Shop\Shop;

/**
 * Class AssetsService
 * @package App\Services\ShopifyAssets
 * @author Sinkevich Alexey
 */
class AssetsService
{
    /**
     * @var AssetAbstract[]
     */
    private $assets = [
        // You can add extra assets here
        ThemeSnippet::class
    ];

    const PLACE_BEFORE = 'before';
    const PLACE_AFTER = 'after';

    /**
     *
     */
    private const SNIPPET_NAME_PATTERN = 'snippets/%s.liquid';

    /**
     * @var ShopifyApi
     */
    private $shopifyApi;

    /**
     * AssetsService constructor.
     * @param ShopifyApi $shopifyApi
     */
    public function __construct(ShopifyApi $shopifyApi)
    {
        $this->shopifyApi = $shopifyApi;
    }

    /**
     * @param Shop $shop
     * @param AssetAbstract ...$onlyAssets
     */
    public function makeAndUpload(Shop $shop, AssetAbstract ...$onlyAssets): void
    {
        $assets = $onlyAssets;
        if (empty($assets)) {
            foreach ($this->assets as $assetClass) {
                if (!is_subclass_of($assetClass, AssetAbstract::class)) {
                    throw new \RuntimeException(sprintf(
                        'Assets must be instances of %s',
                        AssetAbstract::class
                    ));
                }
                $assets[] = app($assetClass);
            }
        }
        foreach ($assets as $asset) {
            $content = $asset->render($shop);

            if ($asset->isSnippet()) {
                // Upload
                $assetName = self::assetName($asset);
                $this->shopifyApi->themes()->assets()->uploadAsset($assetName, $content);

                // Include
                $targetTemplate = $asset->targetTemplate();
                if ($targetTemplate) {
                    $this->shopifyApi->themes()->assets()->includeSnippet(
                        $targetTemplate,
                        self::snippetName($asset),
                        $asset->placePosition() === self::PLACE_BEFORE,
                        $asset->placeSearchText(),
                        self::targetBakupName($asset)
                    );
                }
            } else {
                // add method to Spurit\ShopifyApi\Resource\Assets or
                // copy-paste code from Assets::includeSnippet() to this class and modify it
            }
        }
    }

    /**
     * @param AssetAbstract $asset
     * @return string
     */
    public static function snippetName(AssetAbstract $asset): string
    {
        $assetClass = get_class($asset);
        $parts = explode('\\', $assetClass);
        return sprintf(
            'spurit_%s_%s',
            config('core.mix.app_short'),
            self::from_camel_case(end($parts))
        );
    }

    public function uploadProductSnippet(): void
    {
        $this->shopifyApi->themes()
            ->assets()
            ->put([
            'asset' => [
                'key' => 'snippets/ShopifyProduct.liquid',
                'value' => file_get_contents( base_path('resources/views/liquid/product/ShopifyProduct.liquid')),
            ]
        ]);
        $this->shopifyApi->themes()
            ->assets()
            ->includeSnippet('Templates/product.liquid', 'ShopifyProduct');
    }

    /**
     * @param AssetAbstract $asset
     * @return string
     */
    private static function assetName(AssetAbstract $asset): string
    {
        return sprintf(
            self::SNIPPET_NAME_PATTERN,
            self::snippetName($asset)
        );
    }

    /**
     * @param AssetAbstract $asset
     * @return string
     */
    private static function targetBakupName(AssetAbstract $asset): string
    {
        $ext = '.liquid';
        if (strpos($asset->targetTemplate(), $ext) === false) {
            return '';
        }
        return str_replace(
            $ext,
            '.' . config('core.mix.app_short') . '-backup-' . date('d-m-y_H-i-s') . $ext,
            $asset->targetTemplate()
        );
    }

    /**
     * @param string $input
     * @return string
     */
    private static function from_camel_case(string $input): string
    {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $input));
    }
}
