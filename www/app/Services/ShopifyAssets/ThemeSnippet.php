<?php

declare(strict_types=1);

namespace App\Services\ShopifyAssets;

use Illuminate\Contracts\View\Factory as ViewFactory;
use App\Models\Shop\Shop;

/**
 * Class ThemeSnippet
 * @package App\Services\ShopifyAssets
 * @author Sinkevich Alexey
 */
class ThemeSnippet extends AssetAbstract
{
    /**
     * @var ViewFactory
     */
    private $viewFactory;

    /**
     * ThemeSnippet constructor.
     * @param ViewFactory $viewFactory
     */
    public function __construct(ViewFactory $viewFactory)
    {
        $this->viewFactory = $viewFactory;
    }

    /**
     * @return string
     */
    public function placeSearchText(): string
    {
        return '</body>';
    }

    /**
     * @return string
     */
    public function targetTemplate(): string
    {
        return 'layout/theme.liquid';
    }

    /**
     * @param Shop $shop
     * @return string
     */
    public function render(Shop $shop): string
    {
        return (string)($this->viewFactory->make(
            'snippet.theme-snippet',
            [
                'shopHash' => $shop->getHash(),
                'appName'  => config('core.mix.app_name')
            ]
        ));
    }
}
