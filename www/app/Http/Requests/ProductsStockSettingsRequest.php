<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ProductsStockSettingsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'low_count' => 'integer|nullable',
            'high_count' => 'integer|nullable',
        ];
    }
}