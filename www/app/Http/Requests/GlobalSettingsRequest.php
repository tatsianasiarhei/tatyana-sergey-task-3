<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class GlobalSettingsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'lowProductsCount' => 'required|integer',
            'highProductsCount' => 'required|integer',
        ];
    }
}