<?php

namespace App\Http\Controllers;

use Spurit\PluginCenterApi\ApiInterface as PluginCenterInterface;
use Illuminate\Http\Request;

/**
 * Class PluginCenterController
 * @package App\Http\Controllers
 */
class PluginCenterController extends Controller
{
    /**
     * @var PluginCenterInterface
     */
    private $pcApi;

    /**
     * PluginCenterController constructor.
     * @param Request $request
     * @param PluginCenterInterface $pcApi
     */
    public function __construct(Request $request, PluginCenterInterface $pcApi)
    {
        parent::__construct($request);
        $this->pcApi = $pcApi;
    }

    /**
     *
     */
    public function notifyAboutFirstInstall(): void
    {
        $shop = $this->shop();

        if ($shop->installed) {
            return;
        }

        $shop->installed = true;
        $shop->save();

        $this->pcApi->auth()->setUserId($shop->user_id);
        $this->pcApi->installations()->post();
    }
}
