<?php


namespace App\Http\Controllers\Setting;


use Illuminate\Http\JsonResponse;
use App\Models\Setting\ProductStockSetting;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsStockSettingsRequest;

use Spurit\ShopifyApi\Api as ShopifyApi;

class ProductStockSettingController extends Controller
{
    public function getProductsSettings(int $shopId, ShopifyApi $shopifyApi)
    {
        $productsSettings = new ProductStockSetting();
        return response()->json($productsSettings->mergeWithProducts($shopifyApi->products()->get(), $shopId));
    }

    public function updateProductsSettings(ProductsStockSettingsRequest $request, $shopId): JsonResponse
    {
        $productsSettings = new ProductStockSetting();
        $productsSettings->updateProductsStockSettings($shopId, $request->all());

        return response()->json();
    }
}