<?php


namespace App\Http\Controllers\Setting;


use App\Models\Setting\Settings;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalSettingsRequest;

class GlobalSettingController extends Controller
{
    public function getGlobalSettings(int $shopId): JsonResponse
    {
        return response()->json(Settings::find($shopId)
            ->get(['low_count', 'high_count', 'show_real_count']));
    }

    public function updateGlobalSettings(GlobalSettingsRequest $request, int $shopId): JsonResponse
    {
        $settings = new Settings();
        $settings->updateGlobalSettings($shopId, $request->all());

        return response()->json();
    }
}