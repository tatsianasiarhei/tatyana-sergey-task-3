<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spurit\SelectorPicker\SelectorPicker;

/**
 * Class AdminController
 * @package App\Http\Controllers
 * @author Sinkevich Alexey
 */
class AdminController extends Controller
{
    /**
     * @var $selectorPicker
     */
    private $selectorPicker;

    /**
     * AdminController constructor.
     * @param Request $request
     * @param SelectorPicker $selectorPicker
     */
    public function __construct(Request $request, SelectorPicker $selectorPicker)
    {
        parent::__construct($request);
        $this->selectorPicker = $selectorPicker;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function main()
    {
        \JavaScript::put([
            'initial' => [
                'ids' => [1, 2, 3],
                'is_installation_finished' => (bool) $this->shop()->installed,
            ]
        ]);
        return view('admin');
    }
}
