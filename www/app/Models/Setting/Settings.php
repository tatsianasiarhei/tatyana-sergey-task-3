<?php

namespace App\Models\Setting;

use Carbon\Carbon;
use App\Models;
use App\Events;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Settings
 * @package App\Models
 * @author Sinkevich Alexey
 *
 * @property int $shop_id
 * @property array $config (json)
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Models\Shop\Shop $shop
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Settings extends Model
{
    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [
        'config' => 'array',
        'show_real_count' => 'boolean'
    ];

    /**
     * @var array
     */
    private static $configDefault = [
        'enabled'   => false,
        'selectors' => [
            'add-to-cart' => [
                'auto'     => false,
                'selector' => '',
            ]
        ]
    ];

    /**
     *
     */
    public static function boot(): void
    {
        parent::boot();
        self::created(function (self $setting) {
            event(new Events\OnSettingsSaved($setting->shop));
        });

        self::updated(function (self $setting) {
            event(new Events\OnSettingsSaved($setting->shop));
        });
    }

    /**
     * @return array
     */
    public static function getConfigDefault(): array
    {
        return self::$configDefault;
    }

    /**
     * @param null|string $value
     * @return array
     */
    public function getConfigAttribute(?string $value): array
    {
        $value = !is_null($value) ? json_decode($value, true) : [];
        $value = array_replace_recursive(self::$configDefault, $value);
        return $value;
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Models\Shop\Shop::class);
    }

    public function updateGlobalSettings(int $shopId, array $settingsData): bool
    {
        $this->find($shopId)
            ->update([
                'low_count' => $settingsData['lowProductsCount'],
                'high_count' => $settingsData['highProductsCount'],
                'show_real_count' => $settingsData['showRealProductsCount'],
            ]);

        return true;
    }
}
