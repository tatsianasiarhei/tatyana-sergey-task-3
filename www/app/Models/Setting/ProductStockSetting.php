<?php


namespace App\Models\Setting;


use App\Models\Shop\Shop;
use App\Events\OnSettingsSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductStockSetting extends Model
{
    /**
     * @var string
     */
    protected $table = 'products_stock_settings';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'product_id',
        'low_count',
        'high_count',
        'show_real_count'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'show_real_count' => 'boolean',
    ];

    public static function boot(): void
    {
        parent::boot();
        self::created(function (self $setting) {
            event(new OnSettingsSaved($setting->shop));
        });

        self::updated(function (self $setting) {
            event(new OnSettingsSaved($setting->shop));
        });

        self::deleted(function (self $setting) {
            event(new OnSettingsSaved($setting->shop));
        });
    }

    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

    public function scopeByProductId(Builder $query, int $productId): Builder
    {
        return $query->where('product_id', $productId);
    }

    public function scopeByShopId(Builder $query, string $shopId): Builder
    {
        return $query->where('shop_id', $shopId);
    }

    public function updateProductsStockSettings(int $shopId, array $products): bool
    {
        foreach ($products as $product) {
            $productSettings = $this->byProductId($product['id'])
                ->byShopId($shopId)
                ->updateOrCreate(
                    [
                        'shop_id' => $shopId,
                        'product_id' => $product['id'],
                    ],
                    [
                        'shop_id' => $shopId,
                        'product_id' => $product['id'],
                        'low_count' => $product['low_count'],
                        'high_count' => $product['high_count'],
                        'show_real_count' => $product['show_real_count']
                ]);
        }

        return true;
    }

    public function mergeWithProducts(array $products, int $shopId): array
    {
        $productsSettings = $this->byShopId($shopId)->get();
        $setting = Settings::find($shopId);

        foreach ($products as $product) {
            $product->low_count = $setting->low_count;
            $product->high_count = $setting->high_count;
            $product->show_real_count = $setting->show_real_count;
            foreach ($productsSettings as $productSetting) {
                if ($product->id == $productSetting->product_id) {
                    $product->low_count = $productSetting->low_count;
                    $product->high_count = $productSetting->high_count;
                    $product->show_real_count = $productSetting->show_real_count;
                }
            }
        }

        return $products;
    }

}