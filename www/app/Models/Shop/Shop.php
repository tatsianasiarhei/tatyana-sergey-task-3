<?php

namespace App\Models\Shop;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spurit\Core\Model\Shop as BaseShop;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models;

/**
 * Class Shop
 * @package App\Models\Shop
 *
 * @property int $id
 * @property int $user_id
 * @property string $domain
 * @property string $domain_front
 * @property string $name
 * @property string $email
 * @property string $token
 * @property string $currency
 * @property string $money_format
 * @property string $timezone
 * @property string $hash
 * @property boolean $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property boolean $installed
 *
 * @property Models\Setting\Settings $settings
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Shop extends BaseShop
{
    //use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     *
     */
    public static function boot(): void
    {
        parent::boot();
        self::created(function (self $shop) {
            if (!$shop->settings) {
                $shop->settings()->create();
            }
        });
    }

    /**
     * @return HasOne|Models\Setting\Settings
     */
    public function settings(): HasOne
    {
        return $this->hasOne(Models\Setting\Settings::class, 'shop_id');
    }

    /**
     * @return HasMany|Models\Setting\ProductStockSetting
     */
    public function productStockSetting(): HasMany
    {
        return $this->hasMany(Models\Setting\ProductStockSetting::class, 'shop_id');
    }
}
