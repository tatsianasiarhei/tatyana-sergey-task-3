<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models;

/**
 * Class OnSettingsSaved
 * @package App\Events
 */

class OnSettingsSaved
{
    use SerializesModels;

    /**
     * @var Models\Shop\Shop
     */
    public $shop;

    /**
     * OnSettingsSaved constructor.
     * @param Models\Shop\Shop $shop
     */
    public function __construct(Models\Shop\Shop $shop)
    {
        $this->shop = $shop;
    }
}
