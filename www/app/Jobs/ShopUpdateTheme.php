<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mingalevme\Illuminate\UQueue\Jobs\Uniqueable;

use App\Services\ShopifyAssets\AssetsService;
use App\Models\Shop;

/**
 * Class ShopUpdateTheme
 * @package App\Jobs
 * @author Sinkevich Alexey
 */
class ShopUpdateTheme implements ShouldQueue, Uniqueable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var int
     */
    public $timeout = 60;

    /**
     * @var Shop\Shop
     */
    private $shop;

    /**
     * ShopUpdateTheme constructor.
     * @param Shop\Shop $shop
     */
    public function __construct(Shop\Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function uniqueable(): string
    {
        return md5($this->shop->id);
    }

    /**
     *
     */
    public function handle(): void
    {
        /**
         * @var AssetsService $assetsService
         */
        $assetsService = app(AssetsService::class);
        $assetsService->makeAndUpload($this->shop);
        $assetsService->uploadProductSnippet();
    }
}
