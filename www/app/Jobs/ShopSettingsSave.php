<?php

namespace App\Jobs;

use App\Events\OnSettingsSaved;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mingalevme\Illuminate\UQueue\Jobs\Uniqueable;

use App\Services\AmazonService;
use App\Models\Shop;

/**
 * Class ShopSettingsSave
 * @package App\Jobs
 * @author Sinkevich Alexey
 */
class ShopSettingsSave implements ShouldQueue, Uniqueable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var int
     */
    public $timeout = 15;

    /**
     * @var Shop\Shop
     */
    private $shop;

    /**
     * ShopSettingsSave constructor.
     * @param Shop\Shop $shop
     */
    public function __construct(Shop\Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function uniqueable(): string
    {
        return md5($this->shop->id);
    }

    public function handle(OnSettingsSaved $event): void
    {

        /**
         * @var AmazonService $amazonService
         */
        $amazonService = app(AmazonService::class);
        $amazonService->saveSettings($event->shop);
    }
}
