<?php

namespace App\Jobs;

use App\Events\OnInstall;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mingalevme\Illuminate\UQueue\Jobs\Uniqueable;

use Spurit\RestProxyConnector\RestProxyConnector;
use Spurit\ShopifyApi;
use App\Models\Shop;

/**
 * Class ShopInstall
 * @package App\Jobs
 * @author Sinkevich Alexey
 */
class ShopInstall implements ShouldQueue, Uniqueable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var int
     */
    public $timeout = 30;

    /**
     * @var Shop\Shop
     */
    private $shop;

    /**
     * ShopOnInstall constructor.
     * @param Shop\Shop $shop
     */
    public function __construct(Shop\Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function uniqueable(): string
    {
        return md5($this->shop->id);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(): void
    {
        $this->loadShopDetails();
        event(new OnInstall($this->shop));
        dispatch(new ShopUpdateTheme($this->shop)); // Upload app snippets
//        $this->registerProxyConnector();
    }

    /**
     * Get shop details from Shopify
     */
    private function loadShopDetails(): void
    {
        /**
         * @var ShopifyApi\Api $shopifyApi
         */
        $shopifyApi = app(ShopifyApi\Api::class);
        $shopData = $shopifyApi->shop()->getSingle();
        $this->shop->domain_front = $shopData->currency;
        $this->shop->currency = $shopData->currency;
        $this->shop->money_format = $shopData->money_format;
        $this->shop->timezone = $shopData->iana_timezone;
        $this->shop->save();
    }

    /**
     * Register app instance in RestProxy service (https://bitbucket.org/spuritcompany/api-proxy/src/master/)
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function registerProxyConnector()
    {
        /**
         * @var RestProxyConnector $restProxyConnector
         */
        $restProxyConnector = app(RestProxyConnector::class);
        $restProxyConnector->registerApp(
            config('core.app_id'),
            $this->shop->getDomain(),
            $this->shop->getHash(),
            $this->shop->getToken(),
            ['POST'],
            ['draft_orders']
        );
    }
}
