let mix = require('laravel-mix');

mix.webpackConfig({
    module: {
        rules: [
            { test: /\.scss$/, loaders: ["sass-loader"] },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        // cacheDirectory: true //uncomment it to improve speed
                    }
                }
            }
        ]
    }
});

// App admin assets
mix.react('resources/assets/js/app.js', 'public/assets/app.js')
    .sass('resources/assets/sass/app.scss', 'public/assets/app.css');

// Shop frontend assets
mix.js('resources/cloud/js/load.js', 'public/assets/common.js')
    .sass('resources/cloud/css/common.scss', 'public/assets/common.css');
