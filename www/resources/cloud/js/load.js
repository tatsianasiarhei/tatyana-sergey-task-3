import "core-js/stable";
import "regenerator-runtime/runtime";
import 'element-matches-polyfill';
import 'element-closest-polyfill';

import {Env} from './components/config/env';
import {Settings} from './components/config/settings';
import {Snippet} from './components/config/snippet';
import {Customization} from './components/config/customization';
import {AppConfig} from './components/config/config';

import {AppLibrary} from './components/app-library';
import {Loader} from './components/loader';

import {Application} from './components/application';

const env = new Env();
const APP_NAME = env.appName;

const settings = new Settings(APP_NAME);
const snippet = new Snippet(APP_NAME);
const customization = new Customization(APP_NAME);
const config = new AppConfig(env, settings, snippet, customization);
const library = new AppLibrary(APP_NAME, snippet);

const theGreatLoader = new Loader(library, APP_NAME);

// jQuery
//theGreatLoader.requireJQuery('1.7', '1.9.1');

theGreatLoader.enableSelectorPicker(
    (pageType) => {

      /*if (pageType === 'cart') {
        let _GET = library.requestGetVars();
        if (snippet.cartItems.length !== 1 && typeof(_GET.vid) !== 'undefined') {
          // Add exactly 1 line-item to the cart
          library.ajax('POST', '/cart/clear.js', null, () => {
            library.ajax('POST', '/cart/add.js', 'quantity=1&id=' + _GET.vid, () => {
              document.location.reload();
            });
          });
          return false;
        }
      }*/
      return true;
    },
);

theGreatLoader.addResources(env.awsCommonPath + 'common.css');
theGreatLoader.addResources(env.awsStorePath + snippet.shopHash + '.js');
theGreatLoader.addResources(env.awsStorePath + snippet.shopHash + '.css');
theGreatLoader.addResources('//s3.amazonaws.com/all-apps/js/jquery.spurit.api.js');

/*
// Additional resources for specific page
theGreatLoader.addResources(
    [
      '//s3.amazonaws.com/all-apps/js/spurit.prices.min.js',
      '//s3.amazonaws.com/all-apps/js/spurit.checkout.min.js'
    ],
    ['cart']
);*/

theGreatLoader.run(($, currentPage) => {
  // Run App
  config.init();
  Spurit.global.onReady(
      ['cart', 'checkout', 'atc', 'selectors', 'prices'],
      () => new Application($, config, library, currentPage),
  );
});