const _ = require('lodash');

/* eslint func-names: 0 */
/**
 * @param {string} appName
 * @constructor
 */
export const Settings = function (appName) {
  const defaultSettings = {};

  /**
   *
   */
  this.init = () => {
    const isSettingsSet = (
      typeof (Spurit) !== 'undefined'
      && typeof (Spurit[appName]) !== 'undefined'
      && typeof (Spurit[appName].settings) !== 'undefined'
    );
    let settings = defaultSettings;
    if (isSettingsSet) {
      settings = _.defaultsDeep(_.cloneDeep(Spurit[appName].settings), Spurit[appName].settings);
    }
    for (const key in settings) {
      if ({}.hasOwnProperty.call(settings, key)) {
        this[key] = settings[key];
      }
    }
  };
};