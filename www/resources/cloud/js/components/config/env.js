/* eslint func-names: 0 */
/**
 * @constructor
 */
export const Env = function () {

  /**
   * @type {string}
   */
  this.appName = process.env.MIX_APP_NAME;

  /**
   * @type {string}
   */
  this.awsCommonPath = process.env.MIX_AWS_COMMON_PATH.replace(/\/+$/u, '') + '/';

  /**
   * @type {string}
   */
  this.awsStorePath = process.env.MIX_AWS_STORE_PATH.replace(/\/+$/u, '') + '/';

  /**
   * @type {string}
   */
  this.appApiUrl = process.env.MIX_APP_API_URL;
};