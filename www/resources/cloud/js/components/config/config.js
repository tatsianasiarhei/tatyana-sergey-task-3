/* eslint func-names: 0 */
/**
 * @param {Env} env
 * @param {Settings} settings
 * @param {Snippet} snippet
 * @param {Customization} customization
 * @constructor
 */
export const AppConfig = function (env, settings, snippet, customization) {

  /**
   *
   */
  this.init = () => {
    settings.init();
    this.customization().onConfigReady(this);
  };

  /**
   * @return {Env}
   */
  this.env = () => env;

  /**
   * @return {Settings}
   */
  this.settings = () => settings;

  /**
   * @return {Snippet}
   */
  this.snippet = () => snippet;

  /**
   * @return {Customization}
   */
  this.customization = () => customization;

};
