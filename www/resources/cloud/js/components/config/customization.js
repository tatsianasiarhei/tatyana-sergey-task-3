const _ = require('lodash');

/* eslint func-names: 0 */
/**
 * @param {string} appName
 * @constructor
 */
export const Customization = function (appName) {
  const defaultCustomization = {
    onConfigReady: (config) => null,
    onListenerReady: (listener) => null,
  };

  const isCustomizationSet = (
      typeof (Spurit) !== 'undefined'
      && typeof (Spurit[appName]) !== 'undefined'
      && typeof (Spurit[appName].customization) !== 'undefined'
  );
  let customization = defaultCustomization;
  if (isCustomizationSet) {
    customization = _.defaultsDeep(Spurit[appName].customization, customization);
  }
  for (const key in customization) {
    if ({}.hasOwnProperty.call(customization, key)) {
      this[key] = customization[key];
    }
  }
};