const _ = require('lodash');

/* eslint func-names: 0 */
/**
 * @param {string} appName
 * @constructor
 */
export const Snippet = function (appName) {
  const defaultSnippet = {
    appId: 0,
    shopHash: '',
    moneyFormat: '{{amount_with_comma_separator}}$',
  };

  const isSnippetSet = (
      typeof (Spurit) !== 'undefined'
      && typeof (Spurit[appName]) !== 'undefined'
      && typeof (Spurit[appName].snippet) !== 'undefined'
  );
  let snippet = defaultSnippet;
  if (isSnippetSet) {
    snippet = _.defaultsDeep(Spurit[appName].snippet, snippet);
  }
  for (const key in snippet) {
    if ({}.hasOwnProperty.call(snippet, key)) {
      this[key] = snippet[key];
    }
  }
};