import "core-js/stable";
import "regenerator-runtime/runtime";

import React from 'react';
import { render } from 'react-dom';
import {SpuritApp, isEmbedded} from '@spurit/js-core';

import { routes, PAYMENT, INDEX } from './routes';
import topMenu from './top-menu';
import Storage from './Storage';
import enTranslations from '@shopify/polaris/locales/en.json';
import { Context } from './context';
import ShopifyAppBridge from './ShopifyAppBridge/ShopifyAppBridge';
import ShopifyAppBridgeMock from './ShopifyAppBridge/ShopifyAppBridgeMock';

(() => {
  const {shop, clientId, embedded, ...otherProps} = window.spurit;
  const appBridge = isEmbedded() ? new ShopifyAppBridge(clientId, shop.domain) : new ShopifyAppBridgeMock();
  const storage = new Storage(window.spurit.initial, appBridge);

  render(
    <Context.Provider value={{appBridge, storage}}>
      <SpuritApp
        shop={shop}
        i18n={enTranslations}
        shopifyApiKey={clientId}
        embedded={embedded}
        debug={otherProps.app_env === 'local'}
        icon={document.location.protocol + '//' + document.location.hostname + '/img/icons/spurit-logo-icon.png'}
        isPaid={window.spurit.is_paid /*||true*/}

        routes={routes}
        topMenu={topMenu}
        footer={null}

        mainPageUrl={INDEX}
        paymentPageUrl={PAYMENT}

        {...otherProps}
        appBridge={appBridge}
        storage={storage}
      />
    </Context.Provider>,
    document.getElementById('app'),
  );
})();