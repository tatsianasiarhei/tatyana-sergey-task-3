import PrettyModalMock from './PrettyModalMock';

export default class ShopifyAppBridgeMock {

  /**
   *
   * @param message
   * @param duration
   * @param isError
   */
  toast(message, duration, isError) {
    alert(message);
  }

  /**
   * @return {PrettyModalMock}
   */
  createModal() {
    return new PrettyModalMock(this.app);
  }
}