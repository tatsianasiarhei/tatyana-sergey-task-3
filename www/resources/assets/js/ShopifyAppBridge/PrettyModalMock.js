export default class PrettyModalMock {

  /**
   * {func}
   */
  #open;

  /**
   * {func}
   */
  #close;

  /**
   * @param app
   */
  constructor(app) {
    this.app = app;
  }

  /* eslint no-unused-expressions: 0 */
  /**
   * @param {string} title
   * @param {string} message
   * @param {string} okLabel
   * @param {string} cancelLabel
   */
  create(title, message, okLabel, cancelLabel) {
    setTimeout(() => {
      confirm(message) ? this.#open() : this.#close();
    }, 0);
  }

  /**
   * @param {function} func
   */
  onOpen(func) {
    this.#open = func;
  }

  /**
   * @param {function} func
   */
  onClose(func) {
    this.#close = func;
  }
}