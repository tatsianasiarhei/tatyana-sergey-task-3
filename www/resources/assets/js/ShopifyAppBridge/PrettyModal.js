import {Button, Modal} from '@shopify/app-bridge/actions';

export default class PrettyModal {

  /**
   * {Button}
   */
  #continueButton;

  /**
   * {Button}
   */
  #cancelButton;

  /**
   * {Modal}
   */
  #modal;

  /**
   * @param app
   */
  constructor(app) {
    this.app = app;
  }

  /**
   * @param {string} title
   * @param {string} message
   * @param {string} okLabel
   * @param {string} cancelLabel
   */
  create(title, message, okLabel, cancelLabel) {
    this.#continueButton = Button.create(this.app, {label: okLabel, style: 'danger'});
    this.#cancelButton = Button.create(this.app, {label: cancelLabel});
    this.#modal = Modal.create(this.app, {
      title: title,
      message: message,
      footer: {
        buttons: {
          primary: this.#continueButton,
          secondary: [this.#cancelButton],
        },
      },
    });
    this.#modal.dispatch(Modal.Action.OPEN);
  }

  /**
   * @param {function} func
   */
  onOpen(func) {
    this.#continueButton.subscribe(Button.Action.CLICK, () => {
      func();
      this.#modal.dispatch(Modal.Action.CLOSE);
    });
  }

  /**
   * @param {function} func
   */
  onClose(func) {
    this.#cancelButton.subscribe(Button.Action.CLICK, () => {
      func();
      this.#modal.dispatch(Modal.Action.CLOSE);
    });
  }
}