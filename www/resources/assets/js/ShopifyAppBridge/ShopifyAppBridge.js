import createApp from '@shopify/app-bridge';
import {Toast} from '@shopify/app-bridge/actions';
import PrettyModal from './PrettyModal';

export default class ShopifyAppBridge {

  /**
   *
   * @param apiKey
   * @param shopOrigin
   */
  constructor(apiKey, shopOrigin) {
    this.app = createApp({
      apiKey: apiKey,
      shopOrigin: shopOrigin,
    });
  }

  /**
   *
   * @param message
   * @param duration
   * @param isError
   */
  toast(message, duration, isError) {
    const toastNotice = Toast.create(this.app, {
      message: message,
      duration: duration,
      isError: isError,
    });
    toastNotice.dispatch(Toast.Action.SHOW);
  }

  /**
   * @return {PrettyModal}
   */
  createModal() {
    return new PrettyModal(this.app);
  }
}