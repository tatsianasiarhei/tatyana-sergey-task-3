import {observable, toJS} from 'mobx';
import axios from 'axios';
import {API} from './routes';

const TOAST_DURATION = 5000;

class Storage {

  /**
   * @type {Object}
   */
  initData = {};

  /**
   * @param {{}} initData
   * @param {{}} appBridge
   */
  constructor(initData, appBridge) {
    this.appBridge = appBridge;
    this.initData = initData;
  }

  /**
   * @param context
   */
  set context(context) {
    this._context = context;
  }

  /**
   *
   */
  notifyAboutFirstInstall() {
    axios.post(API.PC.FIRST_INSTALL)
      .catch(error => console.error(error));
  }

  /**
   * @param {Error} error
   */
  error(error) {
    // Safari network error fix
    if (error.message === 'Network Error' && navigator.userAgent.includes('Safari')) {
      return;
    }

    console.error(error.message);
    this.appBridge.toast(error.message, TOAST_DURATION, true);
  }
}

export default Storage;