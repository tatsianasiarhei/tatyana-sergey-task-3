import { API } from './routes';
import axios from 'axios';

export default class Utils {

  /**
   * @todo Use this method to notify PC about first entry in this app
   */
  notifyPluginCenter() {
    axios.post(API.PC.NOTIFY)
    .catch(error => console.error(error));
  }
}