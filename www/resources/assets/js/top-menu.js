import * as route from './routes';

export default [
  {
    content: 'Dashboard',
    url: route.INDEX,
  },
  {
    content: 'Products',
    url: route.PRODUCTS,
  },
  {
    content: 'FAQ',
    url: route.FAQ,
    target: 'new',
  },
  {
    content: 'More',
    actions: [
      {
        content: 'Statistics',
        url: '/stats',
      },
      {
        content: 'Payment',
        url: '/payment',
      },
    ],
  },
];
