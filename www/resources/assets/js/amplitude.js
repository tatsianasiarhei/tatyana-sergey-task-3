import axios from 'axios';
import { API } from './routes';

export const YES = 'yes';


export default class Amplitude {

  /**
   * @param {string} eventName
   * @param {string|null} propertyName
   * @param {string|null} propertyValue
   * @return {Promise<AxiosResponse<any>|void>}
   */
  static sendSimpleEvent(
      eventName,
      propertyName = null,
      propertyValue = null,
  ) {
    const eventProperties = {};

    if (propertyName) {
      eventProperties[propertyName] = propertyValue;
    }
    return this.sendEvent(eventName, eventProperties);
  }

  /**
   * @param {string} eventName
   * @param eventProperties
   * @return {Promise<AxiosResponse<any>|void>}
   */
  static sendEvent(eventName, eventProperties = {}) {
    eventProperties.eventName = eventName;

    return this.#sendEventCustom(eventProperties);
  }

  /**
   * @param {{}} eventProperties
   * @return {Promise<AxiosResponse<any> | void>}
   */
  static #sendEventCustom(eventProperties = {}) {
     return axios.post(API.AMPLITUDE, {
      eventProperties: JSON.stringify(eventProperties),
      userId: window.spurit.shop.user_id,
     })
     .catch(error => this.#error(error));
  }

  /**
   * @param {Error} error
   */
  static #error(error) {
    console.error(error.message);
  }
}