import React, { Component } from 'react';
import { Layout, Card, FormLayout, Checkbox, TextField, Button, Spinner} from '@shopify/polaris';
import axios from "axios";

class Settings extends Component {

  constructor(props) {
    super(props);
    this.state = {
        lowProductsCount: 1,
        highProductsCount: 10,
        showRealProductsCount: false,
        settingsPageIsLoading: true,
    };

    this.updateGlobalSettings = this.updateGlobalSettings.bind(this);
  }

  componentDidMount() {
    axios.get('/global-settings/' + this.props.shop.id)
      .then((response) => {
        this.setState({
          lowProductsCount: response.data[0].low_count,
          highProductsCount: response.data[0].high_count,
          showRealProductsCount: response.data[0].show_real_count,
          settingsPageIsLoading: false
        });
      })
      .catch((error) => {
        console.error(error.message);
      });
  }

  updateGlobalSettings() {
    axios.post('/global-settings/update/' + this.props.shop.id, {...this.state})
      .then(() => {
        alert('Global settings saved');
      })
      .catch((error) => {
        alert('Global settings not saved. Try again.');
        console.error(error.message);
      });
  }

  updateField(field) {
    return (value) => this.setState({
      [field]: value
    });
  }

  render() {
    return (
      <Layout>
        <Layout.Section>
          <Card
            title="Shop Global Settings"
            sectioned
          >
            {this.state.settingsPageIsLoading ?
              <Spinner accessibilityLabel="Spinner example" size="large" color="teal"/>
              :
              <FormLayout>
                <TextField
                  type="number"
                  label="High products count in stock"
                  value={this.state.highProductsCount.toString()}
                  onChange={this.updateField('highProductsCount')}
                />
                <TextField
                  type="number"
                  label="Low products count in stock"
                  value={this.state.lowProductsCount.toString()}
                  onChange={this.updateField('lowProductsCount')}
                />
                <Checkbox
                  type="boolean"
                  label="Show real products count in stock"
                  checked={this.state.showRealProductsCount}
                  onChange={this.updateField('showRealProductsCount')}
                />
              </FormLayout>
            }
          </Card>
        </Layout.Section>
        <Layout.Section>
          {this.state.settingsPageIsLoading ? null : <Button primary onClick={this.updateGlobalSettings}>Update</Button>}
        </Layout.Section>
      </Layout>
    );
  }
}

export default Settings;