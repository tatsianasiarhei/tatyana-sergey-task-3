import React, { Component } from 'react';
import { Layout } from '@shopify/polaris';
import questions from './components/Questions';
import QA from './components/QA';
import { SelectorPicker } from '@spurit/js-selector-picker';

export default class FAQ extends Component {

  /**
   * @param props
   */
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  /**
   * @param selector
   */
  onChange(selector) {
    return console.log(selector);
  }

  /* eslint react/no-array-index-key: 0 */
  /**
   * @return {*}
   */
  render() {
    return (
      <>
        <Layout>
          {questions.map((q, k) => {
            return <QA key={k} {...q}/>;
          })}
        </Layout>
        <SelectorPicker
          label="Select product image on a product page"
          sign={'test'}
          url={'https://' + this.props.shop.domain_front + '/collections/all'}
          withoutUniqueId={true}
          visor={true}
          shopId={this.props.shop.user_id}
          value={'test'}
          onChange={this.onChange}
        />
      </>
    );
  }
}