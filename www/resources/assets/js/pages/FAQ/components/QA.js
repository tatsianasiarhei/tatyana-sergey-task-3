import React from 'react';
import { Layout, Card } from '@shopify/polaris';
import * as PropTypes from 'prop-types';

const QA = (props) => {
  return (
    <Layout.Section>
      <Card
        title={props.question}
        sectioned
      >
        {props.answer}
      </Card>
    </Layout.Section>
  );
};

QA.propTypes = {
  answer: PropTypes.string.isRequired,
  question: PropTypes.string.isRequired,
};

export default QA;