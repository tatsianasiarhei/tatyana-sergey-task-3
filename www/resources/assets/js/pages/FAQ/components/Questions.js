import { Link } from '@shopify/polaris';
import React from 'react';

const questions = [
  {
    question: 'Question?',
    answer: (<strong>Answer</strong>),
  },
  {
    question: 'Question 2?',
    answer: (<p>Please check the <Link url="/">Settings</Link> page</p>),
  },
];
export default questions;