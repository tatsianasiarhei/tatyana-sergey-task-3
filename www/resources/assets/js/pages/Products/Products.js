import React, { Component } from 'react';
import {Layout, Card, Checkbox, Filters, ResourceList, TextField, Button, ResourceItem, TextStyle, Thumbnail, Icon, Spinner} from '@shopify/polaris';
import { ImageMajorTwotone } from '@shopify/polaris-icons';
import axios from 'axios';

export default class Products extends Component {

  /**
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      modifiedProducts: [],
      queryValue: '',
      settingsPageIsLoading: true,
    };

    this.updateSettings = this.updateSettings.bind(this);
  }

  /**
   *
   */
  componentDidMount() {
    axios.get('/products-settings/' + this.props.shop.id)
      .then((response) => {
        this.setState({
          products: response.data,
          settingsPageIsLoading: false,
        });
      })
      .catch((error) => {
        console.error(error.message);
      });
  }

  updateSettings() {
    console.log(this.state.modifiedProducts);
    axios.post('/products-settings/update/' + this.props.shop.id, this.state.modifiedProducts)
      .then(() => {
        alert('Products settings saved');
      })
      .catch((error) => {
        alert('Products settings not saved. Try again.');
        console.error(error.message);
      });
  }

  setQueryValue = (queryValue) => {
    this.setState({
      queryValue: queryValue
    });
  };

  clearQueryValue = () =>
  {
    this.setState({
      queryValue: ''
    });
  };

  updateFields = (id, typeField) => (value) => {
    this.state.products.map((product) => {
      if (product.id == id) product[typeField] = value;
      return product;
    });

    this.updateModifiedProducts(id)
  };

  updateModifiedProducts = (id) => {
    console.log(id)
    const modifiedProduct = this.state.products.find((product) => {
      if (product.id == id) return product;
    });

    const modifiedProducts = this.state.modifiedProducts.filter(product => modifiedProduct.id != product.id)

    this.setState({
      modifiedProducts: [...modifiedProducts, modifiedProduct]
    });
  }

  renderProduct = (product) => {
    const {id, title, image, low_count, high_count, show_real_count} = product;

    const media = image ?
      <Thumbnail source={image.src} alt="Image {title}" size="large"/>
      :
      <Icon source={ImageMajorTwotone} />;

    return (
      <ResourceItem
        id={id}
        media={media}
      >
        <h3>
          <TextStyle variation="strong">{title}</TextStyle>
        </h3>
        <TextField
          type="number"
          label="High products count in stock"
          value={high_count.toString()}
          onChange={this.updateFields(id, 'high_count')}
        />
        <TextField
          type="number"
          label="Low products count in stock"
          value={low_count.toString()}
          onChange={this.updateFields(id, 'low_count')}
        />
        <Checkbox
          type="boolean"
          label="Show real products count in stock"
          checked={show_real_count}
          onChange={this.updateFields(id, 'show_real_count')}
        />
      </ResourceItem>
    );
  }

  render() {

    const filterControl = (
      <Filters
        filters={[]}
        queryValue={this.state.queryValue}
        onQueryChange={this.setQueryValue}
        onQueryClear={this.clearQueryValue}
        onClearAll={this.clearQueryValue}>
      </Filters>
    );

    const products = this.state.products
      .filter(product => this.state.queryValue === '' || product.title.includes(this.state.queryValue));

    return (
      <Layout>
        <Layout.Section>
          <Card>
            {this.state.settingsPageIsLoading ?
              <Spinner accessibilityLabel="Spinner example" size="large" color="teal"/>
              :
              <ResourceList
                resourceName={{singular: 'product', plural: 'products'}}
                items={products}
                filterControl={filterControl}
                renderItem={this.renderProduct}
              />
            }
          </Card>
        </Layout.Section>
        <Layout.Section>
          {this.state.settingsPageIsLoading ? null : <Button primary onClick={this.updateSettings}>Update</Button>}
        </Layout.Section>
      </Layout>
    );
  }
}