import React, {Component} from 'react';
import {Card, SkeletonBodyText} from '@shopify/polaris';
import axios from 'axios/index';

export default class Payment extends Component {

  /**
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = {
      contents: '',
    };
    this.refContainer = React.createRef();
  }

  /**
   *
   */
  componentDidMount() {
    if (!this.state.contents) {
      axios.get('/pc-payment-template')
          .then(response => this.setState({contents: response.data}))
          .catch(error => null);
    }
  }

  /* eslint no-eval: 0 */
  /**
   * @param prevProps
   * @param prevState
   * @param snapshot
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.contents === '' && this.state.contents !== '') {
      if (!this.refContainer.current) {
        return;
      }
      this.refContainer.current.querySelectorAll('script').forEach((script) => {
        eval(script.innerHTML);
      });
    }
  }


  /* eslint react/no-danger: 0 */
  /**
   * @return {*}
   */
  render() {
    if (!this.state.contents) {
      return <Card sectioned><SkeletonBodyText lines={7}/></Card>;
    }
    return (
        <Card sectioned>
          <div
              ref={this.refContainer}
              dangerouslySetInnerHTML={{__html: this.state.contents}}
          />
        </Card>
    );
  }
}