import Settings from './pages/Settings/Settings';
import FAQPage from './pages/FAQ/FAQ';
import {Payment} from './pages/Payment';
import Products from "./pages/Products/Products";

const INDEX = '/';
const FAQ = '/faq';
const PAYMENT = '/payment';
const PRODUCTS = '/products';

const API = {
  PRODUCTS: '/admin/api/products',
  ITEMS_PAGE: '/admin/api/products/page/:id',
  PC: {
    FIRST_INSTALL: '/admin/pc/first-install',
  },
  AMPLITUDE: '/amplitude',
};

const routes = [
  {
    path: INDEX,
    exact: true,
    page: {
      component: Settings,
      title: 'Settings',
      primaryAction: {
        content: 'Save',
        onAction: (design) => console.log('Saved'),
      },
    },
  },
  {
    path: FAQ,
    page: {
      component: FAQPage,
      title: 'FAQ',
    },
  },
  {
    path: PAYMENT,
    page: {
      component: Payment,
      title: 'Payment',
    },
  },
  {
    path: PRODUCTS,
    page: {
      component: Products,
      title: 'Products',
    },
  },
];
export {routes, INDEX, FAQ, PAYMENT, API, PRODUCTS};