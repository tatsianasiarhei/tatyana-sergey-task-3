<?php
/**
 * @var string $encodedSettings
 * @var string $appName
 */
?>
if(typeof Spurit === 'undefined') var Spurit = {};
if(typeof Spurit.<?=$appName?> === 'undefined') Spurit.<?=$appName?> = {};
Spurit.<?=$appName?>.settings = <?= $encodedSettings ?>;

<script src="{{config('filesystems.disks.s3.common_path.url')}}common.js"></script>
<link href="{{config('filesystems.disks.s3.common_path.url')}}common.css" rel="stylesheet" type="text/css" media="all">
