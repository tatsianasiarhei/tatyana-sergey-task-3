<?php
/**
 * @var string $encodedSettings
 * @var string $appName
 */
?>
if(typeof Spurit === 'undefined') var Spurit = {};
if(typeof Spurit.<?=$appName?> === 'undefined') Spurit.<?=$appName?> = {};
Spurit.<?=$appName?>.settings = <?= $encodedSettings ?>;