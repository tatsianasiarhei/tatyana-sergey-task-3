<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5XM56TS');</script>
    <!-- End Google Tag Manager -->

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <style>
        .loader {
            position: relative;
            margin: 0 auto;
            width: 100px;
            height: 100%;
        }
        .loader:before {
            content: '';
            display: block;
            padding-top: 100%;
        }
        .circular {
            -webkit-animation: rotate 2s linear infinite;
            animation: rotate 2s linear infinite;
            height: 100%;
            -webkit-transform-origin: center center;
            transform-origin: center center;
            width: 100%;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }
        .path {
            stroke-dasharray: 1, 200;
            stroke-dashoffset: 0;
            -webkit-animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
            animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
            stroke-linecap: round;
        }

        @-webkit-keyframes rotate {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes rotate {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-webkit-keyframes dash {
            0% {
                stroke-dasharray: 1, 200;
                stroke-dashoffset: 0;
            }
            50% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -35px;
            }
            100% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -124px;
            }
        }
        @keyframes dash {
            0% {
                stroke-dasharray: 1, 200;
                stroke-dashoffset: 0;
            }
            50% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -35px;
            }
            100% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -124px;
            }
        }
        @-webkit-keyframes color {
            100%,
            0% {
                stroke: #d62d20;
            }
            40% {
                stroke: #0057e7;
            }
            66% {
                stroke: #008744;
            }
            80%,
            90% {
                stroke: #ffa700;
            }
        }
        @keyframes color {
            100%,
            0% {
                stroke: #d62d20;
            }
            40% {
                stroke: #0057e7;
            }
            66% {
                stroke: #008744;
            }
            80%,
            90% {
                stroke: #ffa700;
            }
        }
        body {
            background-color: #eee;
        }
        .showbox {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 5%;
        }
    </style>
    <link href="{{ mix('assets/app.css') }}" rel="stylesheet">

    <!-- ClickMagick Pixel Code -->
    {{--<script>
        var cmPixel = document.createElement('img');
        cmPixel.src = "https://www.clkmg.com/api/a/pixel/?uid=108858&att=2&dup=1";
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.clkmg.com/api/a/pixel/?uid=108858&att=2&dup=1" /></noscript>--}}
    <!-- End ClickMagick Pixel Code -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XM56TS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app">
    <div class="showbox">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
        </div>
    </div>
</div>

@include('_footer')

<script src="{{mix('assets/app.js')}}" async></script>

@stack('scripts')

{!! $shop->pcCache->template_menu !!}

</body>
</html>
