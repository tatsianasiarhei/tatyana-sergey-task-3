<?php

use Spurit\ShopifyApi\Api;
use App\Models\Shop;
use App\Jobs;

$config = [
    'app_id'    => env('APP_ID'),
    'namespace' => env('APP_NAMESPACE'),
    'scopes'    => [
        Api::SCOPE_WRITE_THEMES,
        Api::SCOPE_READ_PRODUCTS
    ],
    'oauth'     => [
        'api_key'       => env('OAUTH_API_KEY'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
    ],
    'pc'        => [
        'cookie_name'      => env('PC_COOKIE_NAME'),
        'cookie_key'       => env('PC_COOKIE_KEY', "GZNYBWF\0"),
        'cookie_installed' => env('PC_COOKIE_INSTALLED', ''),
        'url'              => env('PC_URL'),
        'api_url'          => env('PC_API_URL'),
        'api_key'          => env('PC_API_KEY'),
        'api_secret'       => env('PC_API_SECRET'),
        'mock_path'        => env('PC_MOCK_PATH', '/pc-mock'),
        'env'              => env('PC_ENV')
    ],
    'mix'       => [
        'app_name'  => env('MIX_APP_NAME'),
        'app_short' => env('MIX_APP_NAME_SHORT')
    ],
    'embedded'  => env('EMBEDDED_MODE', true),

    'non_free'    => env('PAID_MODE', false),
    'payment_url' => '/payment',

    'shop_model'      => Shop\Shop::class,

    'on_install_jobs' => [
        [
            'job'    => Jobs\ShopInstall::class,
            'queue'  => null,
            'inline' => false
        ]
    ]
];
$config['pc']['admin_cookie_name'] = 'admin-app-logged-' . $config['app_id'];
return $config;
