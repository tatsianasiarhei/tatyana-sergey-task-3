<?php

if (!function_exists('get_amazon_relative_path')) {
    /**
     * @param string $awsFolder   i.e. "appname"
     * @param string $awsFullPath i.e. "https://s3.amazonaws.com/shopify-apps/appname/store/"
     * @return string             i.e. "store/"
     */
    function get_amazon_relative_path(string $awsFolder, string $awsFullPath): string
    {
        $awsFolder = trim($awsFolder, '/');
        $parts = explode($awsFolder . '/', $awsFullPath);
        $result = array_pop($parts);
        return rtrim($result, '/') . '/';
    }
}

return [
    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */
    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */
    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */
    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver'          => 's3',
            'key'             => env('AWS_KEY'),
            'secret'          => env('AWS_SECRET'),
            'region'          => env('AWS_REGION'),
            'bucket'          => env('AWS_BUCKET'),
            'folder'          => env('AWS_FOLDER'),

            // Path to store shop's JS-config and CSS-styles = "store/"
            'store_path'      => get_amazon_relative_path(env('AWS_FOLDER'), env('MIX_AWS_STORE_PATH')),

            'common_path' => [
                'local'  => rtrim(env('LOCAL_COMMON_PATH'), '/') . '/',
                'remote' => get_amazon_relative_path(env('AWS_FOLDER'), env('MIX_AWS_COMMON_PATH')),
                'url'    => rtrim(env('MIX_AWS_COMMON_PATH'), '/') . '/',
                'extensions' => ['css', 'js', 'jpg', 'png', 'gif']
            ],

            'js_libs_path' => get_amazon_relative_path(env('AWS_FOLDER'), env('MIX_AWS_JS_LIBS_PATH'))
        ],
    ],
];
