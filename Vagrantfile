# https://docs.vagrantup.com
# You can search for boxes at https://atlas.hashicorp.com/search

# Project name is folder name
project_name = File.basename(File.dirname(__FILE__))
project_host = "#{project_name}.spurit.loc"
project_host_alias = "www.#{project_name}.spurit.loc"

required_plugins = %w(vagrant-hostmanager)
plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
if not plugins_to_install.empty?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort "Installation of one or more plugins has failed. Aborting."
  end
end

# Version of syntax (do not change)
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/xenial64"
  #config.vm.box_version = "20171028.0.0"
  config.vm.box_check_update = false
  config.vm.define project_name do |t|
  end

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "2500"
	vb.cpus = 2
	vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
  end

  config.vm.synced_folder "./", "/vagrant", id: "vagrant-root",
      owner: "vagrant",
      group: "www-data",
      mount_options: ["dmode=775,fmode=774"]

  config.vm.hostname = project_host
  config.vm.network "private_network", ip: "192.168.20.5" #todo set ip

  # https://github.com/devopsgroup-io/vagrant-hostmanager
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.aliases = project_host_alias

  # Setup web-server
  config.vm.provision "shell", path: ".vagrant-scripts/bootstrap.sh", args: [project_host, project_host_alias]
  config.vm.provision "shell", inline: "service nginx restart", run: "always"
  config.vm.provision "shell", inline: "service php7.3-fpm restart", run: "always"
  config.vm.provision "shell", inline: "chmod 777 -R /var/log/mysql", run: "always"
  config.vm.provision "shell", path: ".vagrant-scripts/tools/mysql-restart.sh", run: "always"
  config.vm.provision "shell", path: ".vagrant-scripts/composer-setup.sh"
  config.vm.provision "shell", path: ".vagrant-scripts/project-setup.sh", privileged: false


  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false
end