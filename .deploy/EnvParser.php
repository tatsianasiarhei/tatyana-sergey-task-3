<?php

namespace Deployer;

/**
 * Class EnvParser
 * @author Nikita Elagin
 */
class EnvParser
{
    /**
     * @param string $localPath
     * @param string $env
     */
    public function keysAreEqual(string $localPath, string $env): void
    {
        $local = array_filter(explode("\n", file_get_contents($localPath)), function ($el) {
            return trim($el);
        });
        $env = explode("\n", $env);

        $remoted = array_filter($env, function ($el) {
            return $el;
        });

        $missingKeys = array_merge(
            array_diff($this->getKeys($local), $this->getKeys($remoted)),
            array_diff($this->getKeys($remoted), $this->getKeys($local))
        );

        $missing = implode(', ', $missingKeys);

        if (!empty($missing)) {
            throw new \RuntimeException('Expected key(s) in .env file is/are missing: ' . $missing);
        }
    }

    /**
     * @param array $lines
     * @return array
     */
    private function getKeys(array $lines): array
    {
        $keys = [];

        foreach ($lines as $line) {
            if ($this->isComment($line) || strpos($line, '=') === false) {
                continue;
            }

            $arr = explode("=", $line);
            if (isset($arr[0])) {
                $keys[] = trim($arr[0]);
            }
        }

        return $keys;
    }

    /**
     * Determine if the line in the file is a comment, e.g. begins with a #.
     * @param string $line
     * @return bool
     */
    private function isComment(string $line): bool
    {
        $line = ltrim($line);
        return isset($line[0]) && $line[0] === '#';
    }
}