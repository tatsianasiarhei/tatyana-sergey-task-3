#!/usr/bin/env bash

set -e

dep_env="${1}"

cd www
if [ $dep_env = "production" ]; then
    cp '.env.ci.prod' '.env'
else
    cp '.env.ci.test' '.env'
fi

COMPOSER_CACHE_DIR=../.composer-cache composer install --ignore-platform-reqs --no-dev --no-ansi -on
#vendor/bin/phpunit --testsuite Unit

#npm set progress = false
#npm install
#
#if [ $dep_env = "production" ]; then
#    # Make non-obfuscated copy of "common.js" and save it to file "common.full.js"
#    npm run dev
#    cd public/assets
#    find . -type f -not -name "common.js*" -print0 |xargs -0 rm
#    mv common.js common.full.js
#    mv common.js.map common.full.js.map
#    cd ../../
#fi
#
#npm run $dep_env

find . -type f -print0 | xargs -0 chmod 744
find . -type d -print0 | xargs -0 chmod 755
cd ..
