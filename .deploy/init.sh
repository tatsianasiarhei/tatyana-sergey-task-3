#!/usr/bin/env bash

mkdir -p ~/.composer
touch ~/.composer/auth.json
echo '{"bitbucket-oauth":{"bitbucket.org":{"consumer-key":"'$BITBUCKET_KEY'","consumer-secret":"'$BITBUCKET_SECRET'"}}}' > ~/.composer/auth.json

touch ~/.npmrc
echo '//registry.npmjs.org/:_authToken='$NPM_TOKEN > ~/.npmrc

mkdir -p ~/.ssh
touch ~/.ssh/testing
touch ~/.ssh/testing.pub
# change env key name
(echo $TESTING_SSH_KEY | base64 --decode -i > ~/.ssh/testing)
(echo $TESTING_SSH_KEY_PUB | base64 --decode -i > ~/.ssh/testing.pub)
chmod 700 ~/.ssh/testing
chmod 700 ~/.ssh/testing.pub

touch ~/.ssh/production
touch ~/.ssh/production.pub
(echo $PRODUCTION_SSH_KEY | base64 --decode -i > ~/.ssh/production)
(echo $PRODUCTION_SSH_KEY_PUB | base64 --decode -i > ~/.ssh/production.pub)
chmod 700 ~/.ssh/production
chmod 700 ~/.ssh/production.pub
