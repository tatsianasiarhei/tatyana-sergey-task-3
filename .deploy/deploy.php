<?php

namespace Deployer;

require 'EnvParser.php';
require 'recipe/common.php';

// Shared files/dirs between deploys
set('shared_files', ['.env']);
set('shared_dirs', [
    'storage/app/public',
    'storage/framework/sessions',
    'storage/logs'
]);
set('clear_paths', ['node_modules']);

// Writable dirs by web server
set('writable_dirs', ['storage', 'bootstrap/cache']);

// Hosts
host('testing', 'production')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->addSshOption('UserKnownHostsFile', '/dev/null');

host('testing')
    ->set('testing', true)
    ->hostname(getenv('TESTING_HOST'))
    ->user(getenv('TESTING_USER'))
    ->port(getenv('TESTING_PORT'))
    ->identityFile(getenv('HOME') . '/.ssh/testing')
    ->set('deploy_path', getenv('TESTING_PATH'));

host('production')
    ->set('testing', false)
    ->hostname(getenv('PRODUCTION_HOST'))
    ->user(getenv('PRODUCTION_USER'))
    ->port(getenv('PRODUCTION_PORT'))
    ->identityFile(getenv('HOME') . '/.ssh/production')
    ->set('deploy_path', getenv('PRODUCTION_PATH'));


task('deploy:upload', function () {
    upload('www/*', '{{release_path}}/');
});


task('deploy:update_code', function () {
    invoke('deploy:writable');
    invoke('deploy:check_composer_requirements');
    invoke('deploy:check_env_file');

    if (get('testing')) {
        //invoke('tests:feature');
    }

    within('{{release_path}}', function () {
        // Execute database migrations
        run('php artisan migrate --force');

        // Upload static files to S3
        if (get('testing')) {
            run('php artisan s3:upload -m');
        } else {
            run('php artisan s3:upload');
        }
    });
});

task('deploy:opcache_reset', function () {
    within('{{release_path}}', function () {
        run('php artisan opcache:reset');
    });
});

task('deploy:check_composer_requirements', function () {
    within('{{release_path}}', function () {
        run('wget -O composer-setup.php https://getcomposer.org/installer');
        run('php composer-setup.php --install-dir=./ --filename=composer');
        run('./composer check-platform-reqs');
        run('rm composer-setup.php');
        run('rm composer');
    });
});

task('deploy:check_env_file', function () {
    within('{{release_path}}', function () {
        (new EnvParser())->keysAreEqual(
            __DIR__ . '/../www/.env.example',
            run('cat .env')
        );
    });
});

task('tests:feature', function () {
    within('{{release_path}}', function () {
        run('vendor/bin/phpunit --testsuite Feature');
    });
});

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:upload',
    'deploy:shared',
    'deploy:update_code',
    'deploy:symlink',
    'deploy:clear_paths',
    'deploy:opcache_reset',
    'deploy:unlock',
    'cleanup',
    'success'
]);

task('rollback:code_only', function () {
    invoke('rollback');
    within('{{release_path}}', function () {
        run('php artisan cache:clear');
        run('php artisan config:clear');
        //upload static files to S3
        run('php artisan s3:upload');
    });
});

task('rollback:with_db', function () {
    invoke('rollback:code_only');
    within('{{release_path}}', function () {
        run('php artisan migrate:rollback');
    });
});

task('deploy:writable', function () {
    within('{{release_path}}', function () {
        $dirs = get('writable_dirs');
        foreach ($dirs as $dir) {
            run("chmod -R -f 775 {$dir}");
        }
    });
});

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
